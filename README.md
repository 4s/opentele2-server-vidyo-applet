Applet for starting VidyoDesktop client
=======================================
This project should be checked out side-by-side with both the opentele-server and
opentele-server-vidyo-plugin projects, so that these directories exist side-by-side:

* opentele-server
* opentele-server-vidyo-plugin
* opentele-server-vidyo-applet

To build and copy the resulting applet to the "dist" folder:

        ant clean dist

To copy the applet to the resulting folder in opentele-server-vidyo-plugin and sign it:

        ant clean deploy

REMEMBER to commit and push the new applet in opentele-server-vidyo-plugin!!!

Please note: Currently the Ant script works only on MacOS. If you need to make it
work somewhere else, you have to edit the line specifying "javaHome" in build.xml.

Manual applet signing
---------------------
If, for some reason, you want to sign the .jar file yourself, use the certificate stored
in the certificates folder.

Do an "ant clean dist" and go to the "dist" folder. Enter

        jarsigner -keystore ../certificates/RegionH\ CodeSign\ OS201306135796.jks -storepass myspass -keypass keypasswd VidyoDesktopClientStarter.jar duke

...where mypass, keypass, and duke should be replaced with the actual values. Have a look
at the properties file in the certificates folder.

The final, signed JAR file should be put in the web-app/applets folder of the
opentele-server-vidyo-plugin project.