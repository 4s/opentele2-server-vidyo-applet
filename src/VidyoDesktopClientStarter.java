import java.applet.Applet;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.AccessController;
import java.security.PrivilegedAction;

import netscape.javascript.*;

public final class VidyoDesktopClientStarter extends Applet {
    private static final String CLIENT_URL_AND_FIRST_PARAMETER = "http://127.0.0.1:63457/dummy?url=http://localhost:8080/dummy&";
    private static final String CLIENT_STATUS_URL = "http://127.0.0.1:63457/?retrieve=epstatus";
    private static final String VEP_STATUS_KEY = "vepState";
    private static final String UNKNOWN_CLIENT_STATUS = "VE_STATUS_UNKNOWN";
    private String clientParameters;
    private String successCallback;
    private String clientNotRunningCallback;

    private String endpointId;
    private boolean clientNotRunning;

    public final void start() {
        clientParameters = getParameter("clientParameters");
        successCallback = getParameter("callback");
        clientNotRunningCallback = getParameter("clientNotRunningCallback");
        fetchEndpointId();
    }

    // Called from JavaScript
    public final void getEndpointId() {
        if (endpointId != null) {
            callJavascriptMethod(successCallback, new Object[] {endpointId});
        } else if (clientNotRunning) {
            callJavascriptMethod(clientNotRunningCallback, new Object[0]);
        }
    }

    public final String getClientStatus() {
        return AccessController.doPrivileged(new PrivilegedAction<String>() {
            @Override
            public String run() {
                try {
                    URLConnection urlConnection = new URL(CLIENT_STATUS_URL).openConnection();
                    urlConnection.setUseCaches(false);
                    InputStream urlInputStream = urlConnection.getInputStream();
                    try {
                        InputStreamReader urlInputStreamReader = new InputStreamReader(urlInputStream);
                        try {
                            BufferedReader bufferedReader = new BufferedReader(urlInputStreamReader);
                            try {
                                return parseClientStatus(bufferedReader.readLine());
                            } finally {
                                bufferedReader.close();
                            }
                        } finally {
                            urlInputStreamReader.close();
                        }
                    } finally {
                        urlInputStream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return UNKNOWN_CLIENT_STATUS;
                }
            }
        });
    }

    private final void callJavascriptMethod(String methodName, Object[] arguments) {
        System.out.println("Calling '" + methodName + "'");

        JSObject win = JSObject.getWindow(this);
        Object result = win.call(methodName, arguments);

        System.out.println("Done. Result from calling '" + methodName + "': '" + result + "'");
    }

    private final void fetchEndpointId() {
        System.out.println("Fetching endpoint ID");

        AccessController.doPrivileged(new PrivilegedAction<Object>() {
            @Override
            public String run() {
                String url = CLIENT_URL_AND_FIRST_PARAMETER + clientParameters;
                System.out.println("URL: '" + url + "'");

                try {
                    String redirectUrl = getRedirect(url);
                    System.out.println("Redirected URL: '" + redirectUrl + "'");

                    endpointId = fetchEndpointIdFrom(redirectUrl);
                } catch (IOException e) {
                    e.printStackTrace();
                    clientNotRunning = true;
                }

                return null;
            }
        });

        System.out.println("Endpoint ID: '" + endpointId + "'");
    }

    private final String getRedirect(String originalUrl) throws IOException {
        URL url = new URL(originalUrl);

        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setConnectTimeout(15000);
        connection.setReadTimeout(15000);
        connection.connect();

        String redirectLocation = connection.getHeaderField("Location");
        if (redirectLocation == null) {
            redirectLocation = connection.getURL().toString();
            System.out.println("Followed redirect to '" + redirectLocation + "'");
        } else {
            System.out.println("Redirect from header: '" + redirectLocation + "'");
        }

        return redirectLocation;
    }

    private final String fetchEndpointIdFrom(String url) {
        String[] urlAndParameters = url.split("\\?");
        String parameters = urlAndParameters[1];
        String[] keysAndParameters = parameters.split("&");

        for (String keyAndParameter : keysAndParameters) {
            String[] keyAndValue = keyAndParameter.split("=");
            String key = keyAndValue[0];
            String value = keyAndValue[1];

            if (key.equals("id")) {
                return value;
            }
        }
        return null;
    }

    private final String parseClientStatus(String statusResponse) {
        String[] keysAndValues = statusResponse.split(",");

        for (String keyAndValue : keysAndValues) {
            String[] keyAndValueSplit = keyAndValue.split("=");
            String key = keyAndValueSplit[0];
            String value = keyAndValueSplit[1];

            if (key.equals(VEP_STATUS_KEY)) {
                return value;
            }
        }
        return UNKNOWN_CLIENT_STATUS;
    }
}
